import java.util.Random;

//mostly recycled code from lab 7

public class CardArray {
    //fields
    private Card[] cards;
    private int pointer;
    private Random rng;

    public CardArray(){
        //initialisation of cards does not happen here
        //since i intend to reuse this function
        //this just initialises some things
 
        this.rng = new Random();
        this.cards = new Card[100];

        //pointer simulates .length value
        this.pointer = 0;
    }

    //expands base array
    //private because it is a helper function
    private void arrayExpansion(){
    //if not, double size
            //first, create new array
            Card[] tempCards = new Card[cards.length * 2];

            //next, copies every thing over from old array
            for(int i = 0; i < this.cards.length; i++){
                tempCards[i] = this.cards[i];
            }

            //finally, sets the new larger to this.cards
            this.cards = tempCards;

    }
    
    //returns size of array
    public int length(){
        return this.pointer;
    }

    //adds card at last space
    public void addCard(Card card){
        //first, checks if there is enough space in array
        if(pointer >= (this.cards.length - 1)){
            arrayExpansion();
            
            cards[pointer] = card;
            //pointer increments last, since base array starts at 0
            this.pointer++;
        }
        else{
            cards[pointer] = card;
            //pointer increments last, since base array starts at 0
            this.pointer++;
        }
        
        
    }

    //replaces card at index
    public void addReplaceCard(Card card, int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            this.cards[index] = card;
        }
        
        
    }

    //takes last card in array
    public Card getLastCard(){
        this.pointer--;
        
		//returns last card, which should be the one the pointer
		return this.cards[pointer];
    }

    //removes specific card at index
    //other cards should scoot over
    public Card removeCardAt(int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            //1. store value of card at i, for later return
            Card returnTemp = this.cards[index];

            //2. copies over next value
            //-1 at length to make sure 
            for(int i = index; i < this.cards.length -1; i++){
                this.cards[i] = cards[i+1];
            }

            //3. reduces value of pointer, since we remove 1 card
            this.pointer--;


            //4. returns the card at index
            return returnTemp;
        }

    }

    //clones card at index, does not remove it from array
    public Card getCloneCard(int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            return this.cards[index];
        }
        
        
    }

    //shuffles cards
    //recycled code from previous labs
    public void shuffle(){
        Card temp;

        //uses pointer instead of array length, 
		//since it marks the "real last element" of the array
        for(int i = 0; i < pointer; i++){
            int randi = rng.nextInt(pointer);

            //saves card at current index in temp
            temp = cards[i];

            //switches curent index with random index
            cards[i] = cards[randi];

            //switches current index with saved random index
            cards[randi] = temp;
        }
    }

    public String toString(){
        String returnString = "";

        for(int i = 0; i < pointer; i++){
            returnString += this.cards[i].getName() + " \n";
        }

        return returnString;
    }

    //print name only of all cards
    public String toStringWithIndex(){
        String returnString = "";

        for(int i = 0; i < pointer; i++){
            returnString += " " + this.cards[i].getName() + "(" + i + ") \n";
        }

        return returnString;
    }

    public int containHowMany(CardType typeIn){
        
        int containsAmount = 0;

        for(int i = 0; i < this.pointer; i++){
            if(this.cards[i].getType() == typeIn){
                containsAmount++;
            }

        }

        return containsAmount;

    }

    //clears the array by setting everything to null

    public void clearCardArray(){
        for(int i = 0; i < this.cards.length; i++){
            this.cards[i] = null;
        }
        //resets pointer
        //VERY IMPORTANT LOL
        this.pointer = 0;
    }

}
