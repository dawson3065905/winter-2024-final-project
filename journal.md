# SHUYUAN YIN - FINAL PROJECT MARKDOWN

## 4-20-2024
- Goal(s):
    - Do CardType enums
    - Do Card class
    - Do CardArray class
    - Finish most functions

- Session achievement(s)
    - CardType enums finished
    - Card class finished
    - CardArray mostly done, add functions
        - constructor
        - length
        - addCard
        - drawLastCard
        - getCardAt
        - shuffle
        - toString (note: split this into two functions)
    - Git repo created

## 4-23-2024
- Goal(s):
    - Double check CardArray class
        - Revise certain functions did in class
    - Add some more functions to CardArray(?)
        - addCardAt
        - contains
    - Ask Eric about design question
        - Enum for description or field
        - Splitting game processor 
    - Begin work on GameProcessor and BuildPile

- Session achievement(s)
	- Refactor CardType Enum
	- Ask question to Eric
	- Add getCloneCard and addReplaceCard functions to CardArray
	- Begin work on GameProcessor

## 4-25-2024
- Goal(s)
    - Automated deck building system
    - Add function to evaluate value of a deck
    - Basic main loop
- Session achievement(s)
    - Evaluate value function partially complete, need to find place to put it

## 4-26-2024
- Goal(s)
    - Complete unfinished goal from last session
- Session achievement(s)
    - Method for evaluating value now functional
    - Methods for building piles now complete
- TODO (New)
    - Main game loop 
    - Refactor getDeckValue, ask friends and Eric for help on Tuesday 
    - Powers

## 4-29-2024
- Goal(s)
    - Refactor GameProcessor
    - Add more calculation stuff to GameProcessor
    - Begin work on main function
- Session achievement(s)
    - GameProcessor refactoring
    - Work on main 
- TODO
    - Main game loop 
    - Powers

## 4-30-2024
- Goal(s)
    - GET MAIN UP AND RUNNING
        - Prints the right things
        - Input validation
- Session achievement(s)
    - Base game now fully functional
- TODO
    - Powers
    - Balancing
    - Intro menu, so people know how the game works

## 5-2-2024
- Goal(s)
    - Make enums for powers
    - Make powers array based on CardArray
- Session achievement(s)
    - Power enums
    - Power array
    - Begin work 
- TODO
    - Need to fix how bacteria is calculated, right now, it does increment by 1 per turn, instead, just gets the num of turns and times it by that. Best to use a private function inside the gameApp
    - Refactor app to use helper function just shoving everything into the main
    - Add more powers
        - DO NOT USE OVERCLOCK, IT DOES NOT WORK SINCE THE VALUE WILL RESET IF THE PLAYER ADDS A NEW CARD
            - Looking into somehow have it trigger only on submit
            - Make it modular, there could be more power in the future that might have this feature
            - Alt solution: dupe all the slag and nugget in the deck
        - Add an temp var to "delay" the next player pile, since clairvoyance will replace it right away
        - Error validation

## 5-3-2024
- Goal(s)
    - Refactor main to use global variable
    - Begin implementing powers button
- Session achievement(s)
    - Code from main now inside reuseable object called "GameManager"
- TODO
    - Add system to build a new pile of power for player
        - Inside either GameProcessor or GameManager
    - Actually implement powers button and powers
        - Giant case block

## 5-4-2024
- Goal(s)
    - Implement powers
- Session achievement(s)
    - Mild refactoring
    - Added functions give cards

## 5-5-2024
- Goal(s)
    - Actually implement powers
- Session achievement(s)
    - Power partially implement
- TODO
    - Get cooldown system fixed
    - Additional powers

## 5-7-2024
- Goal(s)
    - Fix cooldown powers
    - Refactor powers
- Session achievement(s)
    - Powers refactor
    - Cooldowns now works? I think? Needs more testing
- TODO
    - Bug busting