enum CardType{
    NUGGETS("Nugget","+2 points"),
    SLAG("Slag", "+3 points"),
    REFINER("Refiner", "Doubles value of nuggets and slags"),
    GEODE("Geode", "+1 point on its own, value increment by x2 per geode in deck. Max 12 points"),
    CRYSTAL("Crystal", "+0 point on its own, value increments by +4 for every crystal in deck, up to a maximum of 16"),
    STORM("Storm", "+5 points"),
    BACTERIA("Bacteria", "+1 at the beginning of the round, increases by 1 on odd turn"),
    RAIN("Heavy Rain", "+3 points"),
    ELECTRICITY("Static Electricity", "+3 points. If there is storm, +1 to each storm"),
    ACID("Acidic Atmosphere", "+5 points. If there is rain, +2 to each rain, if not, then hard");

    //fields
    private String cardName;
    private String cardDesc;

	//used to set name
    private CardType(String cardName, String cardDesc){
        this.cardName = cardName;
        this.cardDesc = cardDesc;
    }

    public String getName(){
        return this.cardName;
    }

    public String getDesc(){
        return this.cardDesc;
    }
}
