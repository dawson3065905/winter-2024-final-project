public class Power{
    private PowerType powerType;
    private boolean isFirstTimeActivation;
    private int turnSinceLastUse;

    public Power(PowerType powerType){
        this.powerType = powerType;
        this.isFirstTimeActivation = true;
        this.turnSinceLastUse = 0;
    }

    public PowerType getType(){
        return this.powerType;
    }

    public String getName(){
        //just returns the name
        //note: uses function from enum
        return this.powerType.getName();
    }

    public String getDesc(){
        //just returns the name
        //note: uses function from enum
        return this.powerType.getDesc();
    }

    public int getCool(){
        //just returns the name
        //note: uses function from enum
        return this.powerType.getCool();
    }

    //gets the last used turn
    public int getLastUse(){
        return this.turnSinceLastUse;
    }

    //get is it is the first 
    public boolean getFirstTime(){
        return this.isFirstTimeActivation;
    }

    //set last used turn
    public void setLastUse(int turns){
        this.turnSinceLastUse = turns;
    }

    //turns off first activation
    public void setFirstTime(boolean isFirstUse){
        this.isFirstTimeActivation = isFirstUse;
    }


    
	
} 