import java.util.Random;

//note, most of this code is WIP expect major refactoring if i have the time
public class GameProcessor{
    //fields
    private CardArray playerPile;
    private CardArray playerDeck;
    private CardArray hazardHand;
    private PowerArray powerListPlayer;
    private PowerArray powerPile;
    //randomizer
    Random randomizer;
    //array contains all values of enum
    private CardType[] typeArray;

    //maximum cards that pops up in player's hand
    private final int MAXPLAYERHAND; 

    //initialises
    public GameProcessor(){
        this.playerPile = new CardArray();
        this.playerDeck = new CardArray();
        //the hazard's value is always
        this.hazardHand = new CardArray();
        this.randomizer = new Random();

        //array with all the card values, for assignment
        this.typeArray = CardType.values();

        this.MAXPLAYERHAND = 4;

        //builds the power array
        //ok, i decided to put it here because i did want to import random in GameManager
        this.powerListPlayer = new PowerArray();
        this.powerPile = new PowerArray();
    }

    //builds the player pile
    //MAXPLAYERHAND cards in total
    public void buildPlayerPile(){

        //clears dynamic array
        this.playerPile.clearCardArray();

        for(int i = 0; i < MAXPLAYERHAND; i++){
            //bound of 5 because there are currently 5 possible player cards (stored sequentially in enum)
            int typeIndex = randomizer.nextInt(5);
            Card tempCard = new Card(typeArray[typeIndex]);
            this.playerPile.addCard(tempCard);
        }

    }

    //builds the hazards pile
    //takes in rounds for difficulty
    //more rounds = more cards drawn
    public void buildHazPile(int rounds){

        //clears dynamic array
        this.hazardHand.clearCardArray();

        for(int i = 0; i < 3 + rounds; i++){
            int typeIndex = randomizer.nextInt(5)+5;
            Card tempCard = new Card(typeArray[typeIndex]);
            this.hazardHand.addCard(tempCard);
        }

    }

    //shuffles and returns x amount of powers
    public String printXPilePowers(int powerShowAmount){
        this.powerPile.shuffle();

        return this.powerPile.displayPilePowers(powerShowAmount);
    }

    //builds the power pile
    //same code as player pile
    //instead, fills pile with all availabe power
    //avoids duplicates
    //we will just display 3 for the choose instead
    //we will also call it from another helper function
    //still, i left this on public because i might need to just build the pile
    public void buildPowerPile(){

        for(PowerType pType : PowerType.values()){
            Power powerTemp = new Power(pType);

            powerPile.addPower(powerTemp);
        }

    }

    //adds card into player's deck and returns which card was added
    public Card addCardtoHand(int cardIndex){
        //draws card from the player pile
        //based on given index
        Card cardToAdd = this.playerPile.removeCardAt(cardIndex);

        //adds card to deck
        this.playerDeck.addCard(cardToAdd);

        //quick blurb about what card was added
        return cardToAdd;
    }

    //adds power into player's power list and returns which power was added
    public Power addPowertoHand(int powerIndex, int maxDisplay){
        //checks if the player is selecting outside of the x displayed powers
        if(powerIndex < 0 || powerIndex >= maxDisplay)
            throw new IllegalArgumentException("Error: Index is outside of displayed powers");

        //draws power from the power pile
        //based on given index
        Power powerToAdd = this.powerPile.getClonePower(powerIndex);

        //adds power to player's power list
        this.powerListPlayer.addPower(powerToAdd);
        //quick blurb about what power was added
        return powerToAdd;
    }

    //returns various arrays proper
    public CardArray getPlayerHand(){
        return this.playerPile;
    }

    public CardArray getPlayerDeck(){
        return this.playerDeck;
    }

    public CardArray getHazHand(){
        return this.hazardHand;
    }

    //returns the player's hand in string form
    public String getPlayerDeckString(){
        return this.playerDeck.toString();
    }

    //returns the hazard's cards in string form
    public String getHazHandString(){
        return this.hazardHand.toString();
    }

    //returns the player pile's cards in string form
    public String getPlayerHandString(){
        return this.playerPile.toStringWithIndex();
    }

    //returns the value of the player's deck
    public int getPlayerValue(){
        return calculateTotalValue(this.playerDeck);
    }

    //returns the value of haz
    public int getHazValue(){
        return calculateTotalValue(this.hazardHand);
    }

    //returns the list of powers
    public PowerArray getPlayerPowerList(){
        return this.powerListPlayer;
    }

    //clears the player's deck
    public void clearPlayerDeck(){
        this.playerDeck.clearCardArray();
    }

    //for calculating numbers of bacteria
    public int getBacteriaNum(){
        return this.hazardHand.containHowMany(CardType.BACTERIA);
    }

    //resets cooldowns on the powers the player has
    public void resetPlayerPowerCooldowns(){
        for(int i = 0; i < this.powerListPlayer.length(); i++){
            powerListPlayer.resetPowerCooldown(i);
        }
    }

    //setters, mainly for powers

    //sets the playerDeck
    public void setPlayerDeck(CardArray newArray){
        this.playerDeck = newArray;
    }

    //sets the playerPile
    public void setPlayerPile(CardArray newArray){
        this.playerPile = newArray;
    }


    //helper functions

    //returns total points, performs operations using subfunctions
    //more modular(?) design? certainly better than the old approach
    //with its 100 line long function
    private int calculateTotalValue(CardArray inCardArray){
        //returns of different functions
        int slagNuggetPoints = calculateSlagNuggets(inCardArray);
        int crystalPoints = calculateCrystalPoints(inCardArray);
        int geodePoints = calculateGeodePoints(inCardArray);
        //for hazard
        int stormPoints = calculateStormPoints(inCardArray);
        int acidPoints = calculateAcidPoints(inCardArray);

        return slagNuggetPoints + crystalPoints + geodePoints + stormPoints + acidPoints;
    }

    private int calculateSlagNuggets(CardArray inCardArray){
        int nuggetsNum = inCardArray.containHowMany(CardType.NUGGETS);
        int slagNum = inCardArray.containHowMany(CardType.SLAG);
        int refinerNum = inCardArray.containHowMany(CardType.REFINER);
        int slagNuggetPoints = 0;

        if(refinerNum > 0 ){
            slagNuggetPoints = 2*(nuggetsNum*2 + slagNum*3);
        }
        //else, just sums the amount
        else{
            slagNuggetPoints = (nuggetsNum*2 + slagNum*3);
        }

        return slagNuggetPoints;
    }

    private int calculateCrystalPoints(CardArray inCardArray){
        int crystalNum = inCardArray.containHowMany(CardType.CRYSTAL);
        int crystalPoints = 0;

        //calculate points based on num of crystal
        switch (crystalNum){
            case 0: 
                crystalPoints += 0;
                break;
            case 1: 
                crystalPoints += 0;
                break;
            case 2: 
                crystalPoints += 8;
                break;
            default: 
                crystalPoints = crystalNum * 4;
                break;

        }

        //checks for max crystals
        switch (crystalPoints) {
            //if max, return max num of 16
            case 16:
                return 16;
            default:
                return crystalPoints;
        }


    }

    private int calculateGeodePoints(CardArray inCardArray){
        int geodeNum = inCardArray.containHowMany(CardType.GEODE);
        int geodePoints = 0;

        //calculate points based on num of crystal
        switch (geodeNum){
            case 0: 
                geodePoints += 0;
                break;
            case 1: 
                geodePoints += 1;
                break;
            default: 
                geodePoints = geodeNum * 2;
                break;
        }

        //checks for max crystals
        switch (geodePoints) {
            //if max, return number of 12
            case 12:
                return 12;
            default:
                return geodePoints;
        }
    }
    
    //calculate points for storm and rain
    private int calculateStormPoints(CardArray inCardArray){
        int stormNum = inCardArray.containHowMany(CardType.STORM);
        int electricityNum = inCardArray.containHowMany(CardType.ELECTRICITY);
        int totalValue = 0;

        //checks for electricity and its bonuses
        switch (electricityNum){
            case 0:
                //if none, just calculate the value of electricity  
                totalValue += stormNum * 5; 
                break;
            default:
                //calculate hard +3 from electricity
                //plus +6 from each storm (with electricity bonus)
                //if there is no storm, stormNum is 0 and 6 * 0 = 0
                //so we only get the value of electricity
                totalValue += stormNum * 6 + electricityNum * 3;
                break;
         }

        return totalValue;
    }

    //calculate points for acid and rain
    private int calculateAcidPoints(CardArray inCardArray){
        int acidNum = inCardArray.containHowMany(CardType.ACID);
        int rainNum = inCardArray.containHowMany(CardType.RAIN);

        int totalValue = 0;

        //checks for acid and its bonuses
        switch (acidNum){
            case 0:
                totalValue += rainNum * 3; 
                break;
            default:
                totalValue += rainNum * 5 + acidNum * 5;
                break;
        }

        return totalValue;
    }

}