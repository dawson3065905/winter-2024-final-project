import java.util.Scanner;
import java.lang.NullPointerException;

public class GameManager{
    //made this class because
    //1. main is getting too big
    //2. i need to use fields and such

    //fields
    private final int MAXROUNDS;
    private final int MAXTURNS;
    private final int MAXPOWERS;
    private int rounds;
    private int turns;

    private int scoreGoal;
    private int scorePlayer;

    private int hazValue;
    private int playerValue;

    private boolean areYouWinning;
    private boolean isYourTurn;

    private Scanner userScanner;
    private GameProcessor gameProcessor;

    private PowerManager powerManager;

  


    //constructor
    public GameManager(){
        //int for max rounds and max turns
        this.MAXROUNDS = 5;
        this.MAXTURNS = 12;
        this.MAXPOWERS = 3;

        //keeps track of scores and rounds
        //rounds is for outer loop, turns is for inner loop
        this.rounds = 0;
        this.turns = 0;

        //this is the score the player must reach
        this.scoreGoal = 10;
        //this is the player's score
        this.scorePlayer = 0;

        //this is for storing the hazard's value
        //since i will be performing some basic arithmetics on it
        this.hazValue = 0;
        //this is for storing the player's deck's value
        this.playerValue = 0;

        //boolean for determining if player has lost
        this.areYouWinning = true;
        //for deciding when the player turn ends
        this.isYourTurn = true;

        //creates the pile of power players will be using
        //this.powerPile = generatePowerPile();

        //scanner
        this.userScanner = new Scanner(System.in);
        //instantializes gameProcessor
        this.gameProcessor = new GameProcessor();

        this.powerManager = new PowerManager();
    }

    public void startGame(){
        //clears console before rounds
        clearConsole();

        //initializes the power pile
        gameProcessor.buildPowerPile();

        //OUTER LOOP//, ROUNDS
        //5 rounds in total
        while(rounds <= MAXROUNDS && areYouWinning == true){
            //gives prompt to add power to player
            triggerAddPower();

            clearConsole();

            //displays rounds
            System.out.println("Round " + rounds + "\n");

            //builds hazard's deck before 
            gameProcessor.buildHazPile(rounds);
            //get haz value
            hazValue = gameProcessor.getHazValue();
            //player's pile ahead of new round
            gameProcessor.buildPlayerPile();

            ///INNER LOOP///, TURNS
            startTurn();

            //Clears console after each round
            clearConsole();

            //loosing condition 
            if(scorePlayer <= scoreGoal)
                areYouWinning = false;
			else
				System.out.println("\nQuotas met for round " + rounds + "!\n");
            
            //manual garbage collection
            scorePlayer = 0;
            scoreGoal += 2;
            rounds++;
            turns = 0;
            isYourTurn = true;
            gameProcessor.resetPlayerPowerCooldowns();
        }
		
		//checks if player exited the loop because they lost or because they beat the gameProcessor
		if(areYouWinning == false){
			clearConsole();
			System.out.println("\nGAME OVER !\n");
		}
		else{
            clearConsole();
			System.out.println("\nCongrats! You win\n");
		}

    }

    //turns
    public void startTurn(){
        ///INNER LOOP///, TURNS
        while (turns <= MAXTURNS && isYourTurn) {

            //adds bacteria points for this turn
            hazValue += gameProcessor.getBacteriaNum() * (turns%2);

            //prints turns and player score
            System.out.println("Your Score: " + scorePlayer + "\n"
                + "Objective Score: " + scoreGoal + "\n"
                + "Turns: " + turns + " out of " + MAXTURNS + "\n");
            
            //prints hazard's hand
            System.out.println("Hazard Cards: \n" 
                + gameProcessor.getHazHandString() 
                //uses local haz value since that is the one we perform operations on
                + "\nTotal Hazard Score: " + hazValue
                + "\n \n");

            //prints player's deck
            System.out.println("Your deck: \n" 
                + gameProcessor.getPlayerDeckString() 
                + "\nValue of your deck: " + playerValue + "\n");

            //prints player's pile that they can draw from
            System.out.println("Available cards: \n" + gameProcessor.getPlayerHandString());

            //shows possible actions
            System.out.println("Submit (s), End turn (t), Add cards(b), Use power(p)");

            String inputUser = userScanner.nextLine();

            //catches user input error
            boolean isWrongAction = true;

            while(isWrongAction){
                try{
                    switch(inputUser){
                        //submits the card
                        case "s":
                            triggerSubmit();
                            break;
                        case "t":
                            turns++;
                            break;
                        case "b":
                            triggerBuildDeck();
                            break;
                        case "p":
                            tryPowerActivation();
                            break;
                        default: 
                            throw new IllegalArgumentException("Error: Invalid input");
                    }

                    isWrongAction = false;
                }
                catch(NullPointerException e){
                    clearConsole();
                    System.out.println("You do not have any cards in your deck right now");
                    isWrongAction = false;
                }
                catch(Exception e){
                    System.out.println("Invalid input, try again: ");
                    inputUser = userScanner.nextLine();
                }
                
            }
            
            //checks if player has reached required score
            if(scorePlayer >= scoreGoal)
                isYourTurn = false;
            }
    }


    //helpers
    //for "submit"
    private void triggerSubmit(){
        playerValue = gameProcessor.getPlayerValue();
        //checks if player scores
        if(playerValue > hazValue){
            clearConsole();

            scorePlayer += playerValue - hazValue;
            turns++;

            //tells user they have scored
            System.out.println("Scored " + (playerValue - hazValue) + " points\n");

            //resets player value after submission
            gameProcessor.clearPlayerDeck();
            playerValue = 0;

            //resets the hazard since the player value "overwhelmed it"
            gameProcessor.buildHazPile(rounds);
            hazValue = gameProcessor.getHazValue();

        } else if(playerValue == hazValue){
            clearConsole();

            System.out.println("Tie! Scored no points. Hazard points set to 0\n");
            hazValue = hazValue - playerValue;
            turns++;

            gameProcessor.clearPlayerDeck();
            playerValue = 0;

        } else{
            clearConsole();

            System.out.println("Hazard weakened by "+ (hazValue - playerValue) +"\n\n");
            hazValue = hazValue - playerValue;
            turns++;

            gameProcessor.clearPlayerDeck();
            playerValue = 0;
        }
  
    }

    private void triggerBuildDeck(){
        System.out.println("Which card do you want to add: ");
        int playerCardChoice = userScanner.nextInt();
        //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"
        userScanner.nextLine();
                                
        //input validation
        boolean inputCardValid = true;
        while(inputCardValid){
            try{
                Card cardToPrint = gameProcessor.addCardtoHand(playerCardChoice);
                inputCardValid = false;
                //clears console for aesthetics
                clearConsole();
                //quick blurb about what card was added
                System.out.println("You added: " + cardToPrint.getName() + "\n");
            //only catches errors for inputing cards
            }catch(Exception e){
                System.out.println("Please select a card between 0-3, try again: ");
                playerCardChoice = userScanner.nextInt();
                userScanner.nextLine();
            }
    
        }

        turns++;

        //builds new player pile, randomizes cards basically
        gameProcessor.buildPlayerPile();
        playerValue = gameProcessor.getPlayerValue();
        
        
    }


    //triggers event to give player a power
    private void triggerAddPower(){
        System.out.println("Before each round, you get a free power, up to a maximum of " + MAXPOWERS);
        System.out.println(gameProcessor.printXPilePowers(MAXPOWERS));
        System.out.println("You have: " + gameProcessor.getPlayerPowerList());
        System.out.println("Press \'q\' to skip");

        if(gameProcessor.getPlayerPowerList().length() < MAXPOWERS){
            System.out.println("Which power to want to have?: ");

            String playerPowerIndex = userScanner.nextLine();
            //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"

            boolean choiceValidation = true;
            while(choiceValidation){
                try{                      
                    addMorePowers(playerPowerIndex);
                    choiceValidation = false;
                }
                catch(Exception e){
                    System.out.println("Invalid input, try again: ");
                    System.out.println("Which power do you want to have?: ");

                    playerPowerIndex = userScanner.nextLine();
                }
            }
        //if it reaches maximum, just tell the player so
        //if i had more time, i would have added a way to replace instead
        } else {
            System.out.println("Max number of powers reached. Press any key to continue");
            userScanner.nextLine();
        }

    }

    //helpers for function beforehand
    //is one just adds more powers
    private void addMorePowers(String playerPowerIndex){

        if(playerPowerIndex.equals("q")){
            //do nothing
        }else{
            gameProcessor.addPowertoHand(Integer.parseInt(playerPowerIndex), MAXPOWERS);
        }
        
    }


    //checks if power is not cooldown
    private void tryPowerActivation(){
        //if they have no power, tell the player
        if(gameProcessor.getPlayerPowerList().length() <= 0){
            clearConsole();
            System.out.println("You do not have any powers!");
        } else {
       
            System.out.print(gameProcessor.getPlayerPowerList().toStringWithIndex());
            System.out.println("Which power do you want to use: ");
            int playerPowerIndex = userScanner.nextInt();
            //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"
            userScanner.nextLine();

            //gets the power the power player has choosed
            Power powerChoice = gameProcessor.getPlayerPowerList().getClonePower(playerPowerIndex);

            //fat as heck if statement
            //checks if it is not on cooldown by comparing the last used time to the current one
            if(
                //checks if enough turns has elapse since last use (by using substraction)
                powerChoice.getCool() <= turns - powerChoice.getLastUse()
                //OR if it is first time that the get is used this round (this is needed, otherwise the power will not activate on the first round)
                || powerChoice.getFirstTime()
            ){
                //first set the first time to false
                powerChoice.setFirstTime(false);

                //next, set the last used turns
                powerChoice.setLastUse(turns);

                //if the power passes the check activate its powers
                //but first, catches the error if there are no cards in the deck currently
                //since one of the power has a chance of proc'ing it
                activatePower(powerChoice);

            }else{
                //else, informs the player that their power is on cooldown
                clearConsole();
                System.out.println("Power is still on cooldown");
            }

             
        }

    }


    //activates power
    private void activatePower(Power powerChoice){

        switch (powerChoice.getType()) {
            case REPLACE:
                CardArray replaceDeck = powerManager.powerReplace(gameProcessor.getPlayerHand(), gameProcessor.getPlayerDeck()); 
                gameProcessor.setPlayerDeck(replaceDeck);
                break;
            case ALL_CARDS:
                gameProcessor.setPlayerPile(powerManager.powerAllCards());
                break;
            case DRAW_2:
                CardArray drawDeck = powerManager.powerDrawTwo(gameProcessor.getPlayerDeck(), gameProcessor.getPlayerHand());
                gameProcessor.setPlayerDeck(drawDeck);
                break;
            case HALVES_HAZARD:
                this.hazValue = powerManager.powerHalvesHazard(hazValue);
                break;
            case DUPLICATE:
                CardArray dupeDeck = powerManager.powerDuplicate(gameProcessor.getPlayerDeck());
                gameProcessor.setPlayerDeck(dupeDeck);
                break;
            default:
                System.out.println("Error: Invalid power selected");
                break;
        }

    }

    //Clears Console
    private void clearConsole(){
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }
}