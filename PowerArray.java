import java.util.Random;

public class PowerArray {
    //fields
    private Power[] powers;
    private int pointer;
    private Random rng;

    public PowerArray(){
        //initialisation of powers does not happen here
        //since i intend to reuse this function
        //this just initialises some things
 
        this.rng = new Random();
        this.powers = new Power[100];

        //pointer simulates .length value
        this.pointer = 0;
    }

    //expands base array
    //private because it is a helper function
    private void arrayExpansion(){
    //if not, double size
            //first, create new array
            Power[] tempPowers = new Power[powers.length * 2];

            //next, copies every thing over from old array
            for(int i = 0; i < this.powers.length; i++){
                tempPowers[i] = this.powers[i];
            }

            //finally, sets the new larger to this.powers
            this.powers = tempPowers;

    }
    
    //returns size of array
    public int length(){
        return this.pointer;
    }

    //adds power at last space
    public void addPower(Power power){
        //first, checks if there is enough space in array
        if(pointer >= (this.powers.length - 1)){
            arrayExpansion();
            
            powers[pointer] = power;
            //pointer increments last, since base array starts at 0
            this.pointer++;
        }
        else{
            powers[pointer] = power;
            //pointer increments last, since base array starts at 0
            this.pointer++;
        }
        
        
    }

    //replaces power at index
    public void addReplacePower(Power power, int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            this.powers[index] = power;
        }
        
    }

    //takes last power in array
    public Power getLastPower(){
        this.pointer--;
        
		//returns last power, which should be the one the pointer
		return this.powers[pointer];
    }

    //removes specific power at index
    //other powers should scoot over
    public Power removePowerAt(int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            //1. store value of power at i, for later return
            Power returnTemp = this.powers[index];

            //2. copies over next value
            //-1 at length to make sure 
            for(int i = index; i < this.powers.length -1; i++){
                this.powers[i] = powers[i+1];
            }

            //3. reduces value of pointer, since we remove 1 power
            this.pointer--;


            //4. returns the power at index
            return returnTemp;
        }

    }

    //clones power at index, does not remove it from array
    public Power getClonePower(int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        //otherwise, business as usual
        else{
            return this.powers[index];
        }
        
        
    }

    //shuffles powers
    //recycled code from previous labs
    public void shuffle(){
        Power temp;

        //uses pointer instead of array length, 
		//since it marks the "real last element" of the array
        for(int i = 0; i < pointer; i++){
            int randi = rng.nextInt(pointer);

            //saves power at current index in temp
            temp = powers[i];

            //switches curent index with random index
            powers[i] = powers[randi];

            //switches current index with saved random index
            powers[randi] = temp;
        }
    }

    public String toString(){
        String returnString = "";

        for(int i = 0; i < pointer; i++){
            returnString += this.powers[i].getName()
                            + "\n   -" + this.powers[i].getDesc() 
                            + "\n   -Cooldown Turns: " + this.powers[i].getCool() + "\n";
        }

        return returnString;
    }

    //print all powers
    public String toStringWithIndex(){
        String returnString = "";

        for(int i = 0; i < pointer; i++){
            returnString += this.powers[i].getName() + " (" + i + ")"
                            + "\n   -" + this.powers[i].getDesc() 
                            + "\n   -Cooldown Turns: " + this.powers[i].getCool() + "\n";
        }

        return returnString;
    }

    //returns x amount of powers
    //use in selection screen
    public String displayPilePowers(int powerShowAmount){
        String returnString = "";
        //error validation, makes sure you are not trying to display more cards than there is
        if(powerShowAmount < -1 || powerShowAmount >= this.pointer)
            throw new IllegalArgumentException("Error: Value is either too large or below 0");

        for(int i = 0; i < powerShowAmount; i++){
            returnString += this.powers[i].getName() + " (" + i + ")"
                            + "\n   -" + this.powers[i].getDesc() 
                            + "\n   -Cooldown Turns: " + this.powers[i].getCool() + "\n";
        }

        return returnString;
    }

    public int containHowMany(PowerType typeIn){
        
        int containsAmount = 0;

        for(int i = 0; i < this.pointer; i++){
            if(this.powers[i].getType() == typeIn){
                containsAmount++;
            }

        }

        return containsAmount;

    }

    //clears the array by setting everything to null

    public void clearPowerArray(){
        for(int i = 0; i < this.powers.length; i++){
            this.powers[i] = null;
        }
        //resets pointer
        //VERY IMPORTANT LOL
        this.pointer = 0;
    }

    //reset cooldowns of card at index
    public void resetPowerCooldown(int index){
        //checks input throws exception for catch 
        //if bigger than pointer(length) or smaller than 0
        if(index < 0 || index >= pointer){
            throw new IndexOutOfBoundsException("Error: Value is either too large or below 0");
        }
        else{
            powers[index].setFirstTime(true);
            powers[index].setLastUse(0);
        }

    }
}
