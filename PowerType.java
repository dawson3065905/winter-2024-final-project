public enum PowerType {
    HALVES_HAZARD("Micro-Terraformer", "Halves current hazard score", 4),
    //DOUBLE_SYNERGY("Overclocker", "Doubles Refiner bonuses in current hand", 4),
    //PEEK("Clairvoyance", "Let's you see 2 out of 4 cards of your next hand", 4),
    DUPLICATE("Polymorphic Goo", "Randomly duplicates a card in your deck", 4),
    ALL_CARDS("Pure Luck", "Puts every card in your next hand", 4),
    REPLACE("Dump Cargo", "Replaces a card in your current deck of your choice", 4),
    DRAW_2("Additional Equipment", "Draw an additional card from your hand", 5);

    private String powerName;
    private String powerDesc;
    private int cooldown;

    private PowerType(String powerName, String powerDesc, int cooldown){
        this.powerName = powerName;
        this.powerDesc = powerDesc;
        this.cooldown = cooldown;
    }

    public String getName(){
        return this.powerName;
    }

    public String getDesc(){
        return this.powerDesc;
    }

    public int getCool(){
        return this.cooldown;
    }
}
