import java.util.Scanner;

public class HazMiningGame {
    
    public static void main(String[] args){
        Scanner uScanner = new Scanner(System.in);

        System.out.println("Welcome to Hazard Mining!\n");
        System.out.println("You are sent to a uninhabited planet to extract resource and meet quota. To do so, build a deck using these cards:");
        System.out.println("    - Nuggets: +2 points");
        System.out.println("    - Slags: +3 points");
        System.out.println("    - Refiner: Doubles value of nuggets and slags");
        System.out.println("    - Geodes: +1 point on its own, value increment by x2 per geode in deck. Max 12 points");
        System.out.println("    - Crystal: +0 point on its own, value increments by +4 for every crystal in deck, up to a maximum of 16");
        System.out.println("\nOnce you are done, submit you deck and you will score its value.");
        System.out.println("But be careful, there are many hazard will reduce the value of your cards! So be efficient and build your deck well!\n");

        uScanner.nextLine();

        GameManager gameManager = new GameManager();
        gameManager.startGame();
        
    }
 
}
