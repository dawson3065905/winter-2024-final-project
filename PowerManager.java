import java.util.Random;
import java.util.Scanner;

public class PowerManager {
    //decided to move most of the logic here instead
    private Scanner userScanner;

    public PowerManager(){
        this.userScanner = new Scanner(System.in);
    }

    //REPLACE
    //replace a card in the player hand
    public CardArray powerReplace(CardArray playerHand, CardArray playerDeck){
  
        //0. error validation, makes sure there is at least some cards left
        if(playerDeck.length() <= 0 || playerHand.length() <= 0){
            throw new NullPointerException("Error: No cards in either deck or pile to draw from");
        }else{
            //1. asks user which card they want to replace
            System.out.println(playerDeck.toStringWithIndex());
            System.out.println("Which card do you want to replace from your deck?: ");
            int deckIndex = userScanner.nextInt();
            //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"
            userScanner.nextLine();

            //2. ask user which card to replace 
            System.out.println(playerHand.toStringWithIndex());
            System.out.println("Which card do you want to replace it with?: ");
            int handIndex = userScanner.nextInt();
            //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"
            userScanner.nextLine();

            //3. Gets the card from the hand
            Card temp = playerHand.removeCardAt(handIndex);

            //3.5 Stores the card to be replaced, for blurb later
            Card replacedCard = playerDeck.getCloneCard(deckIndex);

            //4. Replaces the card in the deck
            playerDeck.addReplaceCard(temp, deckIndex);

            //5. quick blurb returns output
            System.out.println("You replaced " + temp.getName() + " with " + replacedCard.getName());
            return playerDeck;
        }
    }

    //ALL_CARDS
    //just returns a full deck
    public CardArray powerAllCards(){
        CardType[] allCardTypes = CardType.values();
        CardArray arrayAllCards = new CardArray();

        //uses a for loop to extract all possible player values
        //will maybe refactor to use a check
        //basically, add a flag to the card enums to indicate if it is a player card or not
        //and then use a if statement to only add those that do have that flag
        //maybe next time
        for(int i = 0; i < 5; i++){
            Card temp = new Card(allCardTypes[i]);
            arrayAllCards.addCard(temp);
        }

        return arrayAllCards;
    }

    //DRAW_TWO
    //mods the current player deck
    //note: we are going to use a scanner inside main to get the indices
    public CardArray powerDrawTwo(CardArray playerDeck, CardArray playerHand){
        //no error validation required, most of that is done in CardArray already 

        System.out.println(playerDeck.toStringWithIndex());
        System.out.println("Which card you want to draw: ");
        int inIndex = userScanner.nextInt();
        //bizarre hack fix needed to catch the nextInt, otherwise it "spills over"
        userScanner.nextLine();

        //1. gets the card player wants from the hand
        Card temp = playerHand.removeCardAt(inIndex);
        
        //2. adds the new card to playerDeck
        playerDeck.addCard(temp);

        //3. returns playerDeck with new card
        return playerDeck;
    }

    //HALVES_HAZARD
    //just gets the current hazard value and /2
    //set it in the app class
    public  int powerHalvesHazard(int hazValueIn){
        int newHazValue =  hazValueIn / 2;
        System.out.println("Hazard reduced to " + newHazValue);
        return newHazValue;
    }

    //DOUBLE_SYNERGY
    //recalculates current deck, but the refiner in the current deck are worth
    //code yanked from GameProcessor
    //how the fudge do i implement this?
    public int powerSynergyDouble(CardArray playerDeck){
        int nuggetsNum = playerDeck.containHowMany(CardType.NUGGETS);
        int slagNum = playerDeck.containHowMany(CardType.SLAG);
        int refinerNum = playerDeck.containHowMany(CardType.REFINER);
        int slagNuggetPoints = 0;

        if(refinerNum > 0 ){
            slagNuggetPoints = 4*(nuggetsNum*2 + slagNum*3);
        }
        //else, just sums the amount
        else{
            slagNuggetPoints = (nuggetsNum*2 + slagNum*3);
        }

        return slagNuggetPoints;
    }

    //DUPLICATE
    //randomly duplicates a card from the player's deck
    public CardArray powerDuplicate(CardArray playerDeck){

        if(playerDeck.length() <= 0){
            throw new NullPointerException("No cards in deck to duplicate");
        }else{

            //1. randomly select a card from the playerDeck
            Random randomizer = new Random();

            //2. clones card based on the current deck's lenght
            Card temp = playerDeck.getCloneCard(randomizer.nextInt(playerDeck.length()));

            //3.5 Blurb about which card was duplicated
            System.out.println("Duplicated " + temp.getName());

            //3. adds the card to the playerDeck
            playerDeck.addCard(temp);

            //4. returns playerDeck
            return playerDeck;
        }

    }

}
