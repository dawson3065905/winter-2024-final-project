public class Card{
    private CardType cardType;

    public Card(CardType cardType){
        this.cardType = cardType;
    }

    public CardType getType(){
        return this.cardType;
    }

    public String getName(){
		
        //just returns the name
        //note: uses function from enum
        return this.cardType.getName();
    }
	
} 